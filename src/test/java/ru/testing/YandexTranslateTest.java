package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.testing.entities.Translate;
import ru.testing.gateway.TranslateGateway;

public class YandexTranslateTest {
    private static final String TEXT_TO_TRANSLATE = "Hello World!";
    private static final String LANGUAGE_CODE_EN = "en";
    private static final String LANGUAGE_CODE_RU = "ru";

    @Test
    @DisplayName("Translate to Russian")
    public void testTranslate() throws Exception {
        TranslateGateway translateGateway = new TranslateGateway();
        Translate translater = translateGateway.translate( LANGUAGE_CODE_EN, LANGUAGE_CODE_RU, TEXT_TO_TRANSLATE);
        Assertions.assertEquals( "Всем привет!", translater.getTranslations().get(0).getText());
    }
}
