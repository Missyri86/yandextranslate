package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.Translate;

@Slf4j
public class TranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/detect";
    private static final String URL_LANGUAGES = "https://translate.api.cloud.yandex.net/translate/v2/languages";
    private static final String URL_TRANSLATE = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "";

    @SneakyThrows
    public Translate translate(String sourceLanguageCode, String targetLanguageCode, String texts) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL_TRANSLATE)
                .header("Authorization", TOKEN)
                .queryString("sourceLanguageCode", sourceLanguageCode)
                .queryString("targetLanguageCode", targetLanguageCode)
                .queryString("texts", texts)
                .asString();
        String strResponse = response.getBody();
        log.info("response: " + strResponse);
        return gson.fromJson(strResponse, Translate.class);
    }
}
