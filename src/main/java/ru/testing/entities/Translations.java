package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Translations {
    @SerializedName("text")
    public String text;
}
