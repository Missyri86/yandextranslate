package ru.testing.entities;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
@Generated("jsonschema2pojo")
public class Translate {

    @SerializedName("sourceLanguageCode")
    @Expose
    private String sourceLanguageCode;
    @SerializedName("targetLanguageCode")
    @Expose
    private String targetLanguageCode;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("texts")
    @Expose
    private List<String> texts = null;
    @SerializedName("folderId")
    @Expose
    private String folderId;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("glossaryConfig")
    @Expose
    private GlossaryConfig glossaryConfig;
    @SerializedName("translations")
    @Expose
    private List<Translations> translations;
}
