package ru.testing.entities;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class GlossaryPair {

    @SerializedName("sourceText")
    @Expose
    private String sourceText;
    @SerializedName("translatedText")
    @Expose
    private String translatedText;
}